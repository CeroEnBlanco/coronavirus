﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class Invocador : MonoBehaviour
{
    public GameObject prefab_virus;
    [Space]
    public float frecuencia;
    public float t_virus;

    float t = 0;

    public UnityEvent OnInvoke;

    private void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            GameObject newVirus = (GameObject)Instantiate(prefab_virus, transform.position, transform.rotation);

            newVirus.GetComponent<Virus>().tiempo = t_virus;

            newVirus.GetComponent<Rigidbody2D>().velocity = newVirus.transform.right * 30f;

            OnInvoke.Invoke();
        }
    }

    private void Update()
    {
        t += Time.deltaTime;

        if (t >= frecuencia)
        {
            GameObject newVirus = (GameObject)Instantiate(prefab_virus, transform.position, transform.rotation);

            newVirus.GetComponent<Virus>().tiempo = t_virus;

            newVirus.GetComponent<Rigidbody2D>().velocity = newVirus.transform.right * 30f;

            OnInvoke.Invoke();

            t = 0;
        }
    }
}
