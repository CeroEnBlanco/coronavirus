﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tst : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D rb;
    [SerializeField]
    float velocidad;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            rb.velocity = (pos - transform.position) * velocidad * -1;

            //rb.AddForce((pos - transform.position) * velocidad * -1);
        }
    }
}
