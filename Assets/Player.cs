﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // Cast a ray straight down.
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            // If it hits something...
            if (hit.collider != null)
            {
                if (hit.transform.CompareTag("Virus"))
                {
                    hit.transform.GetComponent<Virus>().Destruir();
                }
            }
        }
    }
}
