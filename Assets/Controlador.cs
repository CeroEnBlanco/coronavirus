﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Controlador : MonoBehaviour
{
    public Player player;
    [Space]
    public int viruses = 0;
    public int maxViruses, contVirusDestruidos;
    [Space]
    public Slider sldrViruses;
    public Text txtViruses, txtVirusDestruidos;
    public GameObject panel_gameOver;

    public bool gameOver = false;

    public float tiempo;
    [Space]
    public Invocador invocador;
    public float t_invocador, vd_invocador, i_invocador;
    [Space]
    public float t_virus;
    public float vd_virus, i_virus;

    private void Start()
    {
        sldrViruses.maxValue = maxViruses;
        sldrViruses.value = 0;

        txtViruses.text = viruses.ToString();

        contVirusDestruidos = 0;
    }

    private void Update()
    {
        if (!gameOver)
        {
            tiempo += Time.deltaTime;

            t_invocador += Time.deltaTime;

            if (t_invocador >= i_invocador)
            {
                invocador.frecuencia -= vd_invocador;

                t_invocador = 0;
            }

            t_virus += Time.deltaTime;

            if (t_virus >= i_virus)
            {
                invocador.t_virus -= vd_virus;

                t_virus = 0;
            }
        }
    }

    public void AumentarContadorVirusDestruidos (int valorEntrante)
    {
        if (valorEntrante > 0)
        {
            contVirusDestruidos += valorEntrante;

            txtVirusDestruidos.text = contVirusDestruidos.ToString();
        }
    }

    public void AunmentarContadorDeViruses ()
    {
        if (!gameOver)
        {
            viruses++;

            sldrViruses.value = viruses;
            txtViruses.text = viruses.ToString();

            if (viruses >= maxViruses)
            {
                gameOver = true;

                invocador.gameObject.SetActive(false);

                player.gameObject.SetActive(false);

                panel_gameOver.SetActive(true);
            }
        }
    }

    public void DisminuirCantidadViruses ()
    {
        if (!gameOver)
        {
            viruses--;

            sldrViruses.value = viruses;
            txtViruses.text = viruses.ToString();
        }
    }

    public void Reiniciar ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
