﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotor : MonoBehaviour
{
    public void Rotar ()
    {
        transform.Rotate(Vector3.forward * (Random.Range(0, 360f)));
    }
}
