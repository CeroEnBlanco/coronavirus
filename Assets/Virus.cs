﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Virus : MonoBehaviour
{
    Controlador controlador;

    public GameObject prefab_virus;
    [Space]
    public float tiempo;

    float t;
    public bool virusGrande = false, copia;

    private void Start()
    {
        controlador = FindObjectOfType<Controlador>();

        t = 0;
    }

    private void Update()
    {
        if (!copia && !virusGrande)
        {
            t += Time.deltaTime;

            if (t >= tiempo)
            {
                Vector3 newScale = transform.localScale;

                newScale.x = 1;
                newScale.y = 1;

                transform.localScale = newScale;

                t = 0;

                virusGrande = true;
            }
        }
    }

    public void Destruir ()
    {
        if (!copia && virusGrande)
        {
            for (int i = 0; i < 8; i++)
            {
                float newRot_z = transform.rotation.z + (45 * i);

                Vector3 newRot = new Vector3(0, 0, newRot_z);

                GameObject newVirus = (GameObject)Instantiate(prefab_virus, transform.position, Quaternion.Euler(newRot));

                newVirus.GetComponent<Rigidbody2D>().velocity = newVirus.transform.right * 10f;

                controlador.AunmentarContadorDeViruses();
            }
        }

        controlador.DisminuirCantidadViruses();

        controlador.AumentarContadorVirusDestruidos(1);

        Destroy(gameObject);
    }
}
